# :construction: Work in progress :construction:

# sts - Storage Tests

The goal of this project is to be able to easily and rapidly write tests for kernel storage drivers and related user-space tools in Fedora, CentOS Stream and Red Hat Enterprise Linux.  

It is designed to work with pytest and [tmt](https://tmt.readthedocs.io/en/stable/). It consists of two main parts:
1) Python [sts-libs](sts_libs/README.md)
2) Tests, Plans with tmt metadata

For contribution guide, please see [docs](docs/contributing.md)
