[build-system]
requires = ['hatchling']
build-backend = 'hatchling.build'

[project]
name = 'sts-libs'
description = ''
readme = 'sts_libs/README.md'
requires-python = '>=3.9'
dynamic = ['version']
authors = [
  {name = 'Bruno Goncalves', email = 'bgoncalv@redhat.com'},
  {name = 'Filip Suba', email = 'fsuba@redhat.com'},
  {name = 'Jakub Krysl', email = 'jkrysl@redhat.com'},
  {name = 'Martin Hoyer', email = 'mhoyer@redhat.com'},
]
maintainers = [
  {name = 'Martin Hoyer', email = 'mhoyer@redhat.com'},
  {name = 'Filip Suba', email = 'fsuba@redhat.com'},
  {name = 'Bruno Goncalves', email = 'bgoncalv@redhat.com'},
]
license = 'GPL-3.0-or-later'
license-files = {paths = ['LICENSE']}
classifiers = [
  'Programming Language :: Python :: 3 :: Only',
  'Programming Language :: Python :: 3.9',
  'Topic :: Software Development :: Testing',
  'Topic :: Software Development :: Quality Assurance',
  'Intended Audience :: Developers',
  'Operating System :: POSIX :: Linux',
  'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
  'Framework :: Hatch',
]
dependencies = [
  'pytest==7.4.3',
  'pytest-testinfra==9.0.0',
  'configobj==5.0.8',
  'six==1.16.0',
    # via configobj
]

[project.urls]
Repository = 'https://gitlab.com/rh-kernel-stqe/sts/'

[tool.hatch.version]
path = 'sts_libs/src/sts/__about__.py'

[tool.hatch.build.targets.sdist]
include = ['sts_libs', 'requirements-stable.txt', 'LICENSE']

[tool.hatch.build.targets.wheel]
packages = ['sts_libs/src/sts']

[dirs.env]
virtual = ".venv"

[tool.hatch.envs.default]
dependencies = [
  "black",
  "ruff",
  "mypy",
]

[tool.hatch.envs.default.scripts]
container-prep = 'buildah from --name fedora-sts fedora && buildah run fedora-sts dnf install -y python3-pip'
tests = 'buildah copy fedora-sts ../sts sts && buildah run fedora-sts pip install sts/ && buildah run fedora-sts pytest sts/sts_libs/tests/'
format = ['ruff --fix {args:.}', 'black {args:.}']
lint = ['ruff {args:.}', 'black --check {args:.}']
check = ['mypy {args:.}']
all = ['format', 'lint', 'check', 'tests']

[tool.black]
line-length = 120
target-version = ['py39', 'py312']
skip-string-normalization = 'True'  # Preferring single quotes

[tool.ruff]
line-length = 120
namespace-packages = ['sts_libs']
src = ['sts_libs/src']
select = [
  'F',  # Pyflakes
  'E',  # pycodestyle
  'W',  # pycodestyle
  #'C90',  # mccabe
  'I',  # isort
  'N',  # pep8-naming
  'D',  # pydocstyle
  'UP',  # pyupgrade
  'YTT',  # flake8-2020
  'ANN',  # flake8-annotations
  #'BLE',  # flake8-blind-except -> TODO
  #'FBT',  # flake8-boolean-trap
  'B',  # flake8-bugbear
  'A',  # flake8-builtins
  'COM',  # flake8-commas
  'C4',  # flake8-comprehensions
  'DTZ',  # flake8-datetimez
  'T10',  # flake8-debugger
  'EM',  # flake8-errmsg
  'ISC',  # flake8-implicit-str-concat
  'ICN',  # flake8-import-conventions
  'G',  # flake8-logging-format
  'INP',  # flake8-no-pep420
  'PIE',  # flake8-pie
  #'T20',  # flake8-print -> TODO
  'PT',  # flake8-pytest-style
  'Q',  # flake8-quotes
  'RSE',  # flake8-raise
  'RET',  # flake8-return
  'SLF',  # flake8-self
  'SLOT',  # flake8-slots
  'SIM',  # flake8-simplify
  'TID',  # flake8-tidy-imports
  'TCH',  # flake8-type-checking
  'INT',  # flake8-gettext
  'ARG',  # flake8-unused-arguments
  'PTH',  # flake8-use-pathlib
  #'ERA',  # eradicate
  'PGH',  # pygrep-hooks
  'PL',  # Pylint
  'TRY',  # tryceratops
  'FLY',  # flynt
  'PERF',  # perflint
  'RUF',  # Ruff-specific rules
]
ignore = [
  'D10',  # Missing docstring -> TODO
  'D205',  # 1 blank line required between summary line and description -> TODO
  'D417',  # Missing argument descriptions -> Do not use docstring description for well type-annotated args
  'PLR09',  # pylint-refactor too-many -> TODO
  'PLR2004',  # magic value -> TODO
  'ANN101',  # Missing type annotation for `self` in method -> Not necessary
  'G004',  # Logging statement uses f-string -> Up for discussion
]

[tool.ruff.per-file-ignores]
'tests/*' = ['INP001']

[tool.ruff.flake8-builtins]
builtins-ignorelist = ['id']

[tool.ruff.flake8-quotes]
inline-quotes = 'single'

[tool.ruff.pydocstyle]
convention = 'google'

[tool.pytest.ini_options]
addopts = ['--verbose']
testpaths = ['sts_libs/tests']

[tool.coverage.report]
omit = ['_test.py']

[tool.mypy]
ignore_missing_imports = true
# strict = true  # TODO -> Will require a lot of effort
show_error_codes = true
python_version = '3.9'

[tool.yamlfix]
explicit_start = 'false'  # '---' at the beggining of the file
line_length = 62  # 62 makes multiline text blocks <= 72 chars
preserve_quotes = 'true'  # otherwise can remove necessary quotes
whitelines = 1 # allow no more than n whiteline(s)
#comments_whitelines = 1
#section_whitelines = 0  # force n whitelines between top-level sections
sequence_style = 'keep_style'  # 'block_style' to force idented lists

[tool.pyright]
pythonVersion = "3.9"
pythonPlatform = "Linux"
reportGeneralTypeIssues = false
